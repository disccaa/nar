import HttpFactory from "~/components/entity/api/factory";


class BaseModule extends HttpFactory {

    getUniversities() {
        return this.call<any>('GET', `universities`)
    }
    getUniversityById(id: string) {
        return this.call<any>('GET', `universities/${id}`)
    }
    getEmployees() {
        return this.call<any>('GET','employees')
    }
    getEmployerById(id:string) {
        return this.call<any>('GET',`employees/${id}`)
    }



}

export default BaseModule
