import type {$Fetch} from 'ofetch'

interface ResponseData<DataResult> {
    hasNextPage: boolean;
    hasPrevPage: boolean;
    limit: number;
    nextPage: number | null;
    page: number;
    pagingCounter: number;
    prevPage: number | null;
    totalDocs: number;
    totalPages: number;
    docs: DataResult
}

class HttpFactory {
    private readonly $fetch: $Fetch

    constructor(fetcher: $Fetch) {
        this.$fetch = fetcher
    }

    /**
     * method - GET, POST, PUT
     * URL
     **/
    async call<T>(method: string, url: string, params?: object, data?: object, extras = {}): Promise<ResponseData<T>> {

        return await this.$fetch(url, {
            method,
            body: data,
            params,
            ...extras,
            onRequest({request, options}) {
            },
            onRequestError({request, options, error}) {
            },
            onResponseError({request, options, response}) {
            }
        })
    }
}

export default HttpFactory
