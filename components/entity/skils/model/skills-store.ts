import Icon1 from '@/components/shared/assets/icons/goals/1.svg'
import Icon2 from '@/components/shared/assets/icons/goals/2.svg'
import Icon3 from '@/components/shared/assets/icons/goals/3.svg'
import Icon4 from '@/components/shared/assets/icons/goals/4.svg'
import Icon5 from '@/components/shared/assets/icons/goals/5.svg'
import Icon6 from '@/components/shared/assets/icons/goals/6.svg'
import Icon7 from '@/components/shared/assets/icons/goals/7.svg'
import Icon8 from '@/components/shared/assets/icons/goals/8.svg'
import Icon9 from '@/components/shared/assets/icons/goals/9.svg'
import Icon10 from '@/components/shared/assets/icons/goals/10.svg'
import Icon11 from '@/components/shared/assets/icons/goals/11.svg'
import Icon12 from '@/components/shared/assets/icons/goals/12.svg'
import Icon13 from '@/components/shared/assets/icons/goals/13.svg'
import Icon14 from '@/components/shared/assets/icons/goals/14.svg'
import Icon15 from '@/components/shared/assets/icons/goals/15.svg'
import Icon16 from '@/components/shared/assets/icons/goals/16.svg'
import Icon17 from '@/components/shared/assets/icons/goals/17.svg'

export const useSkillsStore = defineStore('skills', () => {
    const skills = [
        {id: 1, icon: Icon1},
        {id: 2, icon: Icon2},
        {id: 3, icon: Icon3},
        {id: 4, icon: Icon4},
        {id: 5, icon: Icon5},
        {id: 6, icon: Icon6},
        {id: 7, icon: Icon7},
        {id: 8, icon: Icon8},
        {id: 9, icon: Icon9},
        {id: 10, icon: Icon10},
        {id: 11, icon: Icon11},
        {id: 12, icon: Icon12},
        {id: 13, icon: Icon13},
        {id: 14, icon: Icon14},
        {id: 15, icon: Icon15},
        {id: 16, icon: Icon16},
        {id: 17, icon: Icon17}
    ];
    const getById = (id: number  | string[]) => {
        if (Array.isArray(id)) {
            return skills.filter(skill => id.includes(String(skill.id)));
        } else {
            return skills.find(skill => String(skill.id) === String(id));
        }
    }
    return {
        skills,
        getById
    }

})