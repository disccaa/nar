// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    devServer: {
        host: '127.0.0.1',
    },
    devtools: {enabled: true},
    components: [
        {path: '~/components/shared/ui', prefix: 'Ui'},
        {path: '~/components/entities', prefix: 'Entities'},
        {path: '~/components/features', prefix: 'Features'},
        {path: '~/components/widgets', prefix: 'Widgets'},
        {path: '~/components/entity', prefix: 'Entity'}
    ],
    modules: ['@nuxtjs/tailwindcss', 'nuxt-svgo', "@nuxt/image", '@pinia/nuxt',],
    runtimeConfig: {
        public: {
            apiURL: 'http://localhost:3000/api/', // can be overridden by NUXT_PUBLIC_API_URL environment variable
            mediaURL: 'http://localhost:3000'
        }
    }
})