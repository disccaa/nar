import type {FetchOptions} from 'ofetch'
import {$fetch} from 'ofetch'
import BaseModule from '~/components/entity/api/modules/base'

interface IApiInstance {
    base: BaseModule
}

export default defineNuxtPlugin((nuxtApp) => {
    const baseURL = nuxtApp.$config.public.apiURL
    // && !process.browser
    // ? nuxtApp.$config.public.ssrURL
    // : nuxtApp.$config.public.apiURL


    const fetchOptions: FetchOptions = {baseURL}
    /** create a new instance of $fetcher with custom option */
    const apiFetcher = $fetch.create(fetchOptions)

    /** an object containing all repositories we need to expose */
    const modules: IApiInstance = {
        base: new BaseModule(apiFetcher)
    }

    return {
        provide: {
            api: modules
        }
    }
})
